//
//  ViewController.m
//  ModalNoTab
//
//  Created by Oli Griffiths on 18/10/2013.
//  Copyright (c) 2013 Oli Griffiths. All rights reserved.
//

#import "ViewController.h"
#import "FirstViewController.h"

@interface ViewController ()

@end

@implementation ViewController

-(void)viewDidAppear:(BOOL)animated
{
    FirstViewController *vc1 = [FirstViewController new];
    [self.view addSubview:vc1.view];
}

@end
