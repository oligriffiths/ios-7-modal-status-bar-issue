//
//  FirstViewController.m
//  ModalTabbar
//
//  Created by Oli Griffiths on 18/10/2013.
//  Copyright (c) 2013 Oli Griffiths. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

-(void)viewDidAppear:(BOOL)animated
{
    self.view.backgroundColor = [UIColor redColor];
    
    //Dont touch VC2
    UIViewController *vc2 = [UIViewController new];
    vc2.view.backgroundColor = [UIColor greenColor];
    
    [self presentViewController:vc2 animated:YES completion:nil];
}

@end
